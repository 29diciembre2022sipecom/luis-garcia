# Luis García

# Comando Docker para levantamiento de base de datos 

docker run -d -p 3306:3306 --name mysql-docker-container -e MYSQL_DATABASE=telconet -e MYSQL_USER=telconet -e MYSQL_PASSWORD=telconet mysql/mysql-server:latest

Dicho comando es el encargado de realizar la conexion a la base de datos MYSQL definida dentro del properties del aplicativo

# Postman 
    El proyecto cuenta con una coleccion de prueba de cada path del servicio rest definido. Alojado dentro de la ruta telconet/src/main/resources 
    Telconet.postman_collection.json

# aplicaction properties
PATH.URL.LOAD -> variable encargada de parametrizar la url para la carga masiva

# Path de consumos de servicios

    Cada path para ser ejecutado tiene que ir acompañado del prefijo
    localhost:8080/
    * Para las categorias
        -> category/save
        -> category/edit
        -> category/delete/{id}
        -> category/listar
        -> category/listarId/{id}
    Siendo {id} una variable dinamica del tipo integer que seria el identificador de cada categoria y/o producto

    * Para los productos 
        -> product/cargaMasiva
        -> product/delete/{id}
        -> product/product/edit
        -> product/editImage
        -> product/findProduct/{id}

# Puerto de consumo
    * 8080

# Formato de proyecto
    Archivo compilado: telconet-0.0.1-SNAPSHOT.war
    alojado en la carpeta target

    Dicho proyecto puede ser desplegado en un servidor del tipo Jboos, Para asi ser consumidos los servicios fuera del ambiente local.