package com.com.telconet.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.com.telconet.demo.entities.Category;
import com.com.telconet.demo.services.CategoryService;


@Controller
@RequestMapping("category")
public class CategoryController {
	
	@Autowired
	CategoryService categoryService;
	
	/**
	 * 
	 * @param category
	 * @return
	 */
	@PostMapping("/save")
	private ResponseEntity<?> save(@RequestBody Category category){
		
		return new ResponseEntity<>(categoryService.saveCategory(category), HttpStatus.OK);
	}
	 /**
	  * 
	  * @param category
	  * @return
	  */
	@PutMapping("/edit")
	private ResponseEntity<?> edit(@RequestBody Category category){
		return new ResponseEntity<>(categoryService.editCategory(category), HttpStatus.OK);
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@DeleteMapping("delete/{id}")
	private ResponseEntity<?> delete(@PathVariable Integer id){
		
		if(categoryService.deleteCategory(id)) {
			return new ResponseEntity<>("Categoria Eliminado", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("La categoria ya se encuentra eliminado", HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 
	 * @return
	 */
	@GetMapping("/listar")
	private ResponseEntity<?> findByCategory(){
		return new ResponseEntity<>(categoryService.listCategory(), HttpStatus.OK);
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/listarId/{id}")
	private ResponseEntity<?> findByCategory(@PathVariable Integer id){
		return new ResponseEntity<>(categoryService.findByCategory(id), HttpStatus.OK);
	}
	

}
