package com.com.telconet.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.com.telconet.demo.entities.Product;
import com.com.telconet.demo.services.ProductService;

@Controller
@RequestMapping("product")
public class ProductController {

	@Autowired
	ProductService productService;

	/**
	 * 
	 * Carga masiva de productos en la base de datos
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/cargaMasiva")
	private ResponseEntity<?> loadMasive() throws Exception {

		return new ResponseEntity<>(productService.loadProduct(), HttpStatus.OK);
	}

	/**
	 * @param id
	 * @return
	 */
	@DeleteMapping("delete/{id}")
	private ResponseEntity<?> deleteProduct(@PathVariable Integer id) {

		if (productService.deleteProduct(id)) {
			return new ResponseEntity<>("Producto Eliminado", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("El producto ya se encuentra eliminado", HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * 
	 * @param product
	 * @return
	 */
	@PostMapping("/edit")
	private ResponseEntity<?> editProduct(@RequestBody Product product) {
		return new ResponseEntity<>(productService.editProduct(product), HttpStatus.OK);
	}

	/**
	 * 
	 * @param id
	 * @param image
	 * @return
	 */
	@PutMapping("/editImage")
	private ResponseEntity<?> editImage(@RequestParam Integer id, @RequestParam String image) {

		return new ResponseEntity<>(productService.editImageProduct(id, image), HttpStatus.OK);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/findProduct/{id}")
	private ResponseEntity<?> findProduct(@PathVariable Integer id) {

		return new ResponseEntity<>(productService.findProductId(id), HttpStatus.OK);

	}

}
